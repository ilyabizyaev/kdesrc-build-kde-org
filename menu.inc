<?php
// Update this in site.inc as well!!!
$current_version = '-git';

$this->setName ("kdesrc-build Homepage");

/* The third parameter of append link should be
   false for external links.
 */
$section =& $this->appendSection("Inform");
$section->appendLink("Home","");
$section->appendLink("KDE Home","http://www.kde.org/",false);

$section =& $this->appendSection("Resources");
$section->appendLink("kdesrc-build Build Guide", "http://techbase.kde.org/Getting_Started/Build/kdesvn-build", false);
$section->appendLink("Documentation", "https://docs.kde.org/index.php?application=kdesrc-build&language=en", false);
$section->appendLink("Syntax highlighting for KWrite and Kate", "other/kate-syntax-highlight.php");

//$section->appendLink("Configuring the shell to use the new KDE", "documentation/shell-settings.php");

?>
