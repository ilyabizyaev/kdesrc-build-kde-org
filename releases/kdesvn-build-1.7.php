<?php

  $release_version = '1.7';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2008-Oct-05</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.6.2.php">1.6.2</a>):</p>

<h3>Features:</h3>
<ul>
  <li>A persistent data store for kdesvn-build to store little bits of data about modules.  Right
    now it is used to automatically re-run CMake or configure when you change the options (i.e.
    you don't have to specify --reconfigure yourself anymore).  Also this is used to print out a
    warning message when a module fails to build more than 3 times in a row as that is probably
    a problem on the user's computer (for instance, a missing required program).</li>

  <li>The <tt><a
    href="http://kdesvn-build.kde.org/documentation/kdesvn-buildrc.html#conf-apidox">apidox</a></tt>
    option has been removed, and has never worked for the KDE 4 series.  See
    the <a href="http://techbase.kde.org/Development/Tools/apidox">apidox
    Techbase page</a> for information on how to build the KDE API documentation
    on your computer.</li>

  <li>The "virtual module" phonon was introduced in the last kdesvn-build release to allow you
    to easily build Phonon 4.2 (required for KDE 4.1).  Now you can specify the "branch trunk"
    option for this virtual module and kdesvn-build will still do the right thing.</li>

  <li>kdesvn-build will now try harder to choose an appropriate default module branch if you've
    specified a global KDE branch.  For instance if you try to build KDE 4.1, when the "phonon"
    module is built it will be pulled from its 4.2 branch (KDE 4.1 requires Phonon 4.2 or
    higher).</li>

  <li>The way the "latest" symlink in the log directory is generated has been
    changed.  Now instead of a symlink pointing to the latest log directory, the
    latest directory contains a list of symlinks for the individual modules.
    What this means is that if you want to see the results of your last kdebase
    build but you only build qt-copy on your last kdesvn-build run, you'll
    actually see the results of your last kdebase build instead of "no directory
    named kdebase".</li>

  <li>Allow double-quoted options in <tt><a
    href="http://kdesvn-build.kde.org/documentation/kdesvn-buildrc.html#conf-cmake-options">cmake-options</a></tt>.
    Patch provided by Alain Boyer.  See <a
    href="http://bugs.kde.org/show_bug.cgi?id=171814">KDE bug 171814</a>.</li>

  <li>Standard kdesvn-buildrc-sample updates.</li>
</ul>

<h3>Bugfixes:</h3>
<ul>
  <li>Make the <tt><a href="http://kdesvn-build.kde.org/documentation/kdesvn-buildrc.html#conf-do-not-compile">do-not-compile</a></tt>
    option work for KDE 4 modules for the first time.  This requires a bit of support from the
    underlying build system (macro_optional_add_subdirectory) but this is fairly widespread by
    now.</li>

  <li>Fix l10n build.</li>
  <li>Fix <a href="http://bugs.kde.org/show_bug.cgi?id=168860">KDE bug 168860</a> where source
    code conflicts already present in a module would cause the build to fail with misleading
    build log errors.  Source code conflicts are only expected in the qt-copy module when using
    the <tt><a href="http://kdesvn-build.kde.org/documentation/kdesvn-buildrc.html#conf-apply-patches">apply-patches</a></tt>
    option but are checked for in any case.  When conflicts are detected the user is notified
    and told how to correct.  Correction is a manual job as conflicts can also occur if you are
    working on the source code.</li>

  <li>Fix some incorrect variable names in a few error messages.</li>
  <li>Fix <a href="http://bugs.kde.org/show_bug.cgi?id=171248">KDE bug 171248</a> involving
    incorrect CMAKE_CXX_FLAGS syntax.  Patch provided by Daniel Richard G.  Note that the
    underlying <tt><a href="http://kdesvn-build.kde.org/documentation/kdesvn-buildrc.html#conf-cxxflags">cxxflags</a></tt>
    option also requires setting CMAKE_BUILD_TYPE=none in your cmake-options.  See the cxxflags
    documentation for more details.</li>

  <li>Fix <a href="http://bugs.kde.org/show_bug.cgi?id=172110">KDE bug 172110</a> where
    kdesvn-build would unnecessarily download a trunk module snapshot when the
    <tt><a href="http://kdesvn-build.kde.org/documentation/kdesvn-buildrc.html#conf-module-base-path">module-base-path</a></tt>
    option was used.</li>
</ul>

<?php
  include("footer.inc");
?>
