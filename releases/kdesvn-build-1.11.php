<?php

  $release_version = '1.11';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2009-Dec-24</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.10.php">1.10</a>):</p>

<h3>New features/changes:</h3>
<ul>
  <li>Git module support has been improved. Specifically, kdesvn-build will try as much
  as possible to use existing user-created remote names and remote-tracking branch names
  instead of always creating new remote aliases or remote-tracking branches.</li>

  <li>The <a href="/documentation/">Documentation</a> page has been overhauled. It is
  still out-of-date in some spots, but is much closer. The <a
  href="/documentation/conf-options-table.html">Table of configuration file options</a>
  and <a href="/documentation/supported-cmdline-params.html">list of command-line
  parameters</a> should be accurate now at least.</li>

  <li>The <a
    href="/documentation/conf-options-table.html#conf-configure-flags">configure-flags</a>
    option for the <tt>qt-copy</tt> module no longer includes
    <tt>-no-exceptions</tt> since that disables some useful Qt functionality. It is
    recommended you remove it from your own kdesvn-buildrc if you have it. In addition the
    <tt>-qdbus</tt> configure flag for qt-copy is now <tt>-dbus</tt>.</li>

  <li>Environment variable changes are no longer output in <tt>--pretend --verbose</tt>.
  If you need to see the changes kdesvn-build will make to the environment, use
  <a href="/documentation/supported-cmdline-params.html#cmdline-debug">--debug</a>.</li>

  <li>The <a href="/documentation/developer-features.html#ssh-agent-reminder">SSH Agent
    check</a> is only performed if a module is being updated that uses SSH (including Git
    modules that use SSH).</li>

  <li>kdesvn-build checks to ensure the known KDE SSL signature is in use, this check is
    only performed for Subversion modules (since it's not needed for Git modules).</li>
</ul>

<h3>Bugfixes:</h3>
<ul>
  <li>The
    <a href="/documentation/supported-cmdline-params.html#cmdline-verbose">--verbose</a>
    option now works in asynchronous mode (the default).</li>

  <li>The <a href="/other/kate-syntax-highlight.php">KatePart syntax highlighting info</a>
  (used by KDE platform text editors) has been updated to be current.</li>
</ul>

<?php
  include("footer.inc");
?>
