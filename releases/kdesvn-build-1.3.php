<?php

  $release_version = '1.3';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2006-Oct-10</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p>Most of the work in the 1.3 release came from my fellow KDE developers, who took care
of kdesvn-build while I was away getting trained up.  So I'd like to say thanks to them.</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.2.php">1.2</a>):</p>

<h3>Bugfixes</h3>
<ul>
<li>When using the --run command line option, all command line parameters are
   passed to the given command.</li>
<li>Qt's /lib directory is added to the pkg-config search path by default now,
   for Qt-DBus</li>
<li>kdesvn-build no longer incorrectly whines about module checkout locations
   different than expected.  (If you still get this warning, it should actually
   be true now. ;)</li>
<li>kdepimlibs is built by default in KDE 4 mode.  If you're using a
   ~/.kdesvn-buildrc file, you need to add it manually however.</li>
<li>qt-copy default options are set correctly.</li>
<li>make-options are now used when installing the module as well, with the
   exception that the -j option (for parallel building) is stripped out.</li>
<li>make install is no longer run for qt-copy if qt-copy is already in position.</li>
<li>Lots of minor output and bug fixes.</li>
</ul>

<h3>Feature Additions</h3>
<ul>
<li>The kde4-snapshot branch name is no longer recognized, as all KDE 4
   development is against the /trunk version of kdelibs now.</li>
<li>Many, many improvements to the parts of kdesvn-build used by the Coverity
   scanner.  Much of the code has been extracted into a separate helper script
   to avoid clutter as well.</li>
<li>kdesvn-build will automatically perform any necessary svn switch routines
   for you (if you use the --svn-only switch).  This is handy if you've changed
   the branch or download location for a module you've already checked out.</li>
<li>Added progress output support for CMake 2.4.3.  Now there is progress output
   with both KDE 3 and KDE 4 builds.</li>
</ul>

<?php
  include("footer.inc");
?>
