<?php
  $release_version = '1.13';
  $release_title = "kdesrc-build $release_version";
  $release_file = "kdesrc-build-$release_version.tar.bz2";
  $page_title = "kdesrc-build release $release_version";
  $site_root = "../";

  include("header.inc");
?>

<p>Released: <b>2011-Feb-27</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.12.1.php">1.12.1</a>):</p>
<!-- Excuse the HTML below, I got tired of writing it and simply used a
Markdown-to-HTML converter -->
<h3>New features/changes:</h3>
<ul>
<li><p>The change with perhaps the biggest difference has been the migration of
kdesrc-build to git.kde.org. kdesrc-build no longer tries to install itself,
but will still install documentation and the Kate XML syntax highlighting
file. The assistance of the KDE sysadmins, localization team, Nicol&aacute;s
Alvarez, Pino Toscano, and Stas Verberkt were invaluable for successfully
setting up the new repository.</p></li>

<li><p>Many updates from David Faure and Montel Laurent to try and keep the
kdesrc-buildrc-sample in sync with the continuing changes to the KDE source
repository layout. <em>Please ensure</em> that you compare your current
~/.kdesrc-buildrc with the provided sample as some modules have moved, or are
currently moving from Subversion to git. In addition, the Qt library with KDE
modifications has moved to git.kde.org as well, and is called qt-kde.  <strong>You
should still leave it as "module qt-copy" in your ~/.kdesrc-buildrc</strong>, as
currently kdesrc-build uses that module name for the different logic needed
to build Qt.</p></li>
<li><p>kdesrc-build now supports using the database behind <a href="http://projects.kde.org/">KDE
Projects</a>. The idea is that the many git
repositories hosted on git.kde.org are arranged into categories (e.g.
kdegraphics, kdepim, calligra, etc.) at the KDE Projects site, and these
categories can be further grouped. kdesrc-build can use a category name to
find and build every git repository contained in that category to allow you
to easily specify many modules to be built, and track new applications added
to a category automatically.  Allen Winter contributed the core database
parsing code needed for this feature.</p>

<p>This feature uses the <a href="http://kdesrc-build.kde.org/documentation/kde-modules-and-selection.html#module-sets">module-set</a> concept added in
1.12.1. In order to use the KDE module database, you must use <code>kde-project</code>
as the value to the <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-repository">repository</a> option for a module-set.
You would then use the <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-use-modules">use-modules</a> option as normal to
select what modules (or module groups) you want kdesrc-build to build.</p></li>
<li><p>module-sets can now be given names, and those names may be used from the
command line to quickly refer to the entire set.</p></li>
<li><p>The <a href="http://kdesrc-build.kde.org/documentation/supported-cmdline-params.html#cmdline-resume-from">--resume-from</a> and
<a href="http://kdesrc-build.kde.org/documentation/supported-cmdline-params.html#cmdline-resume-after">--resume-after</a> options no longer inhibit source
updating. If you really want to avoid source code updates when using either
option, simply pass <a href="http://kdesrc-build.kde.org/documentation/supported-cmdline-params.html#cmdline-no-src">--no-src</a> as well.</p></li>
<li><p>Default CMake build type is changed to "Debug", which includes debug info,
but still provides most optimizations. Thanks to "guy-kde" for the report!</p></li>
<li><p>Some documentation improvements, including for the new KDE module database
support.</p></li>
<li><p>git-diff is not run in pretend mode. (Doing so made the pretended output more
accurate, but ends up being fairly slow).</p></li>
<li><p>To support the KDE Projects module database, kdesrc-build has changed a bit
the way modules are handled and read internally. I think I've caught all the
bugs by now, but if you have problems with what kdesrc-build is trying to do
with a given module, it may be related to this reworking and should be
reported as a bug.</p></li>
<li><p>Added a test suite (kdesrc-build-test.pl), which you can run to test some
various assumptions used for kdesrc-build. <em>The test suite is not at all
comprehensive</em>. Some kdesrc-build changes were merely to make some of the
functions easier to test.</p></li>
</ul>

<h3>Bugfixes:</h3>
<ul>
<li><p>The last-built and last-installed revisions are now saved for git modules in
addition to svn modules. This should extend the "don't build if the source
didn't change" feature to git modules as well.</p></li>
<li><p>The kdesrc-buildrc-sample now contains a Phonon backend by default
(phonon-gstreamer). Phonon requires at least one backend to work -- it
doesn't have to be phonon-gstreamer, but if you don't want that one then be
sure to include a different one. Thanks to Ghislain MARY for the report.
(<a href="https://bugs.kde.org/show_bug.cgi?id=263937">Bug 263937</a>).</p></li>
<li><p>Improve the error message if kdesrc-build finds itself trying to build a
module that doesn't have either a CMake or configure-based build system.</p></li>
<li><p>More exception error messages are actually output when they occur.</p></li>
<li><p>Consistency updates to <a href="http://kdesrc-build.kde.org/documentation/supported-cmdline-params.html#cmdline-pretend">--pretend</a> output.</p></li>
<li><p>kdesrc-build no longer tries to invent matching tag names for the kdesupport
module to match a given KDE branch, as kdesupport is mostly migrated to git,
and KDE is not generating any new tags to correspond to a given KDE Platform
release.</p></li>
<li><p>Updated internal list of default modules to be closer to matching what is
needed to build the KDE Platform and Plasma Desktop.</p></li>
<li><p>kdesrc-build will avoid using a source control tool for an existing source
directory that is different than the tool used to create the directory, even
if it is known that the module for that directory has moved to git.  What
this means for you is that if kdesrc-build has errors updating or building a
module, you may want to check if that module has moved to git. Likewise, if a
module doesn't exist, kdesrc-build will guess what source control tool to use
based on if the <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-svn-server">svn-server</a> or
<a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-repository">repository</a> options are set.</p></li>
<li><p><a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-set-env">set-env</a> now works in module-sets.</p></li>
<li><p>Completely unknown module or module set names now result in an error instead
of the script exiting after making no output.</p></li>
</ul>

<?php
  include("footer.inc");
?>
