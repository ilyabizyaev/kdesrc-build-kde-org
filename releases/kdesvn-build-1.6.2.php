<?php

  $release_version = '1.6.2';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2008-Jul-11</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.6.1.php">1.6.1</a>):</p>

<h3>Bugfixes:</h3>
<ul>
  <li>Corrected the build order of phonon and kdesupport in the sample configuration file.  If
  you are building phonon using the functionality added in 1.6.1 you must ensure that it comes
  after kdesupport, especially for new installs.
  (<a href="http://bugs.kde.org/show_bug.cgi?id=166308">KDE bug 166308</a>).</li>
  <li>Improved internal option handling to make the set_option() warning that would sometimes
  appear go away. (<a href="http://bugs.kde.org/show_bug.cgi?id=157978">KDE bug 157978</a>)</li>
  <li>Don't force qt-copy to assume that the yacc program (used for automatic parser generation)
  is byacc.  If qt-copy fails to build without this change it is probably a bug in Qt's
  configure script.</li>
  <li>Fix the BUILD_WITH_phonon option in the sample configuration file to read BUILD_phonon=
  instead.</li>
  <li>Rename the kdeplasmoids module to kdeplasma-addons where it is referenced in kdesvn-build.
  If you have the kdeplasmoids module set to build in your configuration you should rename it
  to kdeplasma-addons as the KDE Subversion module has also been renamed.</li>
  <li>Do not warn when trying to build "l10n", which is the pseudo module you can use to force
  building the translations when you have <a
  href="/documentation/kdesvn-buildrc.html#conf-kde-languages">kde-languages</a> enabled.</li>
</ul>

<?php
  include("footer.inc");
?>
