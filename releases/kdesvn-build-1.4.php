<?php

  $release_version = '1.4';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2007-May-03</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.3.php">1.3</a>):</p>

<h3>Bugfixes</h3>
<ul>
<li>kdesupport is built by default now, as it is now required for KDE 4.</li>
<li>The rm -rf command is no longer used.  The rm -rf feature is implemented
natively in Perl to improve portability and possibly improve security.</li>
<li>The documentation has been revised/updated, including the DocBook sources,
the man page sources, and the help text.</li>
<li>Do not check for SSH Agent in pretend mode.</li>
<li>The Net::HTTP module is not required to run, it is only required to use the
snapshot downloading feature.</li>
<li>The branch and tag option does not apply to qt-copy when set in the global
options now.</li>
<li>When the use-stable-kde option is false assume CMake is being used.  This
prevents needless updates of kdenonbeta and kde-common for unsermake
support.</li>
<li>Outdated references to kde4-snapshot have been removed.</li>
<li>kdesvn-build no longer assumes that it knows what SVN repository the
snapshots were configured for.</li>
<li>Do not run a checkout for a module where the snapshot was successfully
installed but the follow-on update failed.</li>
</ul>

<h3>Feature Additions</h3>
<ul>
<li>Added the ability to perform the initial checkout of trunk KDE modules by
downloading a snapshot from ftp.kde.org.  This is done before checking the
kdesvn-build.kde.org site, which is less frequently updated.</li>
<li>The use-cmake option has been removed.  CMake use has not been optional for
quite a few months now.</li>
<li>The default module list and options have been reviewed and updated.
kdesvn-build should be ready to build KDE 4 with less tweaking needed.</li>
</ul>

<?php
  include("footer.inc");
?>
