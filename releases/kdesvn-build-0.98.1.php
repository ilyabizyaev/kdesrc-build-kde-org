<?php

  $release_version = '0.98.1';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p><?php echo $release_title; ?> is on the <b>KDE 4.0</b> development track.  If you intend to build KDE 3 with this release, it is possible, but it would be much easier to use <a href="kdesvn-build-<?php echo $current_version; ?>.php">kdesvn-build <?php echo $current_version;?></a> instead.
</p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release:</p>

<h3>Bugfixes</h3>
<ul>

<li>Fixed bug where kdesvn-build did not support the line continuation character
(\) properly.  The sample configuration file has been adjusted to use the
character.
</li>

<li>The sample configuration file now works with Qt 4.</li>

<li>Fix <a href="http://bugs.kde.org/show_bug.cgi?id=111167">bug 111167</a>, now kdesvn-build will warn you if the directory a module was checked out from is different from what it should be (which can happen if you change the branch or tag option without re-checking out).</li>

<li>The /admin directory symlink is now re-created whenever a module is built or installed, previously it was only re-created when running make -f Makefile.cvs, which caused install errors on some systems.</li>

<li>Fix a regression where setting empty options no longer worked.  You can now set an option to an empty value again.</li>

<li>-march=i686 setting removed from default configuration as it interferes with non-32-bit x86 systems.  It is still included in the sample configuration file for you to configure.</li>

</ul>

<h3>Feature Additions</h3>
<ul>

<li><p>kdesvn-build can now use the following syntax in the configuration file to
avoid having to retype options: kdesvn-build will subtitute sequences like ${variable-name} with the value of variable-name.  This substitution is done when the sequence is first encountered so the option must be defined first.
</p>

<p>For example, you could do something like the following:
<pre>global
  source-dir ~/kdesvn
  qtdir ${source-dir}/build/qt-copy
end global</pre></p>

<p>Whenever you change source-dir, qtdir will automatically reflect the change.</p>
</li>

</ul>

<?php
  include("footer.inc");
?>
