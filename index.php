<?php
  $site_root = './';
  include("header.inc");
?>

<img src="kdesrc-build-icon.png" alt="kdesrc-logo, image of test tubes with bubbling fluid" style="float: left; display: inline;  margin-right: 8pt; margin-bottom: 12pt;"/>

<b>kdesrc-build</b> is a tool to allow you to easily build <a href="http://www.kde.org/">KDE</a> from its source repositories.

<br><br>

<b>Latest Release:</b>: The development version of kdesrc-build is currently
recommended, and can be downloaded from KDE's git server by running:

<pre> $ git clone git://anongit.kde.org/kdesrc-build
</pre>

This command will download kdesrc-build (the script and its associated modules)
and sample configuration files suitable for KDE Frameworks 5 and Plasma Desktop
5.

Afterwards, you can run <pre>kdesrc-build --metadata-only</pre> to have
kdesrc-build download the current information about the KDE source
configuration.

From there you can try <pre>kdesrc-build --pretend</pre> to see what
kdesrc-build would do, or something like <pre>kdesrc-build --include-dependencies plasma-desktop</pre>
to build Plasma Desktop 5 and its associated kde.org-based dependencies.
<br clear="all">

<h3>Features</h3>
<ul>
<li>Will automatically checkout and update the KDE source code from its
source repository. Both Subversion and git repository types are supported.</li>

<li>Integrates with the KDE source code infrastructure to automatically build modules
in the required order, and using the appropriate branch.</li>

<li>Easily supports building specific branches, tags, or even revisions of a module (or all
of KDE).</li>

<li>Supports many build systems. It's even possible to build many non-KDE software
projects in a pinch (for instance, CMake can be built from its own git sources if
your distribution does not have CMake packages).</li>

<li>Supports speedy initial checkouts of modules (KDE Git projects only) by
using the snapshots already available on the KDE project
network.</li>

<li>Supports a "dry run" mode (the --pretend) option so that you can experiment
with different settings non-destructively.</li>

<li>kdesrc-build can download modules (both initial checkout and updates) even
while building modules that have already been updated.</li>

<li>kdesrc-build supports uninstalling modules (manually, or prior to installing
an already-installed module) in order to keep the install directory clean. Note that
this feature requires CMake support and is still experimental.</li>

<li>kdesrc-build logs everything for easy perusal later, that way you can
determine why things went wrong if a build fails.  kdesrc-build automatically
creates symlinks for easy access to the last log (log/latest).</li>

<li>color-coding of the output (which can be turned off)</li>

<li>Has support for building the Qt Project's <a
href="http://qt-project.org/">Qt</a> library, which is a prerequisite for KDE
software.</li>

<li>kdesrc-build is very customizable.  You can control most options down to a
module-by-module basis if you so desire, including configure-flags, CMake
flags, and your C++ flags during compilation.  Instead of trying to remember
what configure line you used, you can set it once and forget it.</li>

<li>Extensive <a href="documentation/">documentation</a>.  Feel free to let me know
if you need something explained (e-mail address at bottom), or contact the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">kde-devel mailing list</a>.</li>

<li>A detailed sample configuration file is included, usually you can just copy
it to ~/.kdesrc-buildrc and be done with it.</li>

<li>Much more!</li>

</ul>

<?php
  include("footer.inc");
?>
